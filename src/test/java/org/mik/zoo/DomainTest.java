//XDF26K
package org.mik.zoo;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.lang.reflect.Modifier;

import org.junit.Test;
import org.mik.zoo.animal.AbstractAnimal;
import org.mik.zoo.animal.Animal;
import org.mik.zoo.animal.AnimalType;
import org.mik.zoo.animal.bird.Bird;
import org.mik.zoo.animal.fish.Fish;
import org.mik.zoo.animal.mammal.AbstractMammal;
import org.mik.zoo.animal.mammal.Elephant;
import org.mik.zoo.animal.mammal.Mammal;
import org.mik.zoo.factory.ZooCsvFactory;

/**
 * @author Maya Gillilan E0FAAA
 *
 */
public class DomainTest {
	
	public final static String T_SCINAME = "Ursus Arctos";
	public final static String T_IN_NAME = "Instance";
	public final static String T_IMG = "URL";
	public final static int NO_OF_LEGS = 4;
	public final static int NO_OF_TEETH = 30;
	public final static int AVG_WEIGHT = 3000;
	public final static AnimalType AN_TYPE = AnimalType.PREDATOR;
	public final static int HAIR_LEN = 4;
	public final static String OTH_SCINAME = "Caretta Caretta";
	@Test
	public void test() {
		assertTrue(LivingBeing.class.isInterface());
		assertTrue(Animal.class.isInterface());
		assertTrue(Bird.class.isInterface());
		assertTrue(Fish.class.isInterface());
		assertTrue(Mammal.class.isInterface());
		assertTrue(Modifier.isAbstract(AbstractLivingBeing.class.getModifiers()));
		assertTrue(Modifier.isAbstract(AbstractAnimal.class.getModifiers()));
		assertTrue(Modifier.isAbstract(AbstractMammal.class.getModifiers()));
		assertFalse(Modifier.isAbstract(Elephant.class.getModifiers()));
		assertFalse(Modifier.isAbstract(ZooCsvFactory.class.getModifiers()));
		try {
			assertNotNull(AbstractLivingBeing.class.getMethod("getScientificName")); //$NON-NLS-1$
			assertNotNull(AbstractLivingBeing.class.getMethod("setScientificName", String.class)); //$NON-NLS-1$
			assertNotNull(AbstractAnimal.class.getMethod("getNumberOfLegs")); //$NON-NLS-1$
			assertNotNull(AbstractAnimal.class.getMethod("getNumberOfTeeth")); //$NON-NLS-1$
			assertNotNull(AbstractAnimal.class.getMethod("getAverageWeight")); //$NON-NLS-1$]
			assertNotNull(AbstractAnimal.class.getMethod("getAnimalType")); //$NON-NLS-1$
			assertNotNull(AbstractAnimal.class.getMethod("setNumberOfLegs", int.class)); //$NON-NLS-1$
			assertNotNull(AbstractAnimal.class.getMethod("setNumberOfTeeth", int.class)); //$NON-NLS-1$
			assertNotNull(AbstractAnimal.class.getMethod("setAverageWeight", int.class)); //$NON-NLS-1$
			assertNotNull(AbstractAnimal.class.getMethod("setAnimalType", AnimalType.class)); //$NON-NLS-1$
			assertNotNull(AbstractAnimal.class.getMethod("isFish")); //$NON-NLS-1$
			assertNotNull(AbstractAnimal.class.getMethod("isMammal")); //$NON-NLS-1$
			assertNotNull(AbstractAnimal.class.getMethod("isBird")); //$NON-NLS-1$
			assertNotNull(AbstractMammal.class.getMethod("getHairLength")); //$NON-NLS-1$
			assertNotNull(AbstractMammal.class.getMethod("setHairLength", int.class)); //$NON-NLS-1$
		}
		catch (Exception e) {
			fail(e.getMessage());
		}
		Elephant be = new Elephant(T_SCINAME, T_IN_NAME, T_IMG, NO_OF_LEGS, NO_OF_TEETH, AVG_WEIGHT, AN_TYPE, HAIR_LEN);
		/**assertEquals(st.getName(), T_NAME);
		assertEquals(st.getBirthYear(), T_YEAR);
		assertEquals(st.getSpecialization(), T_SPEC);
		Student st1 = new Student(T_NAME, T_YEAR, T_SPEC);
		assertFalse(st==st1);
		assertEquals(st, st1);
		st1.setName(OTHER_NAME);
		assertNotEquals(st, st1);
		st.setName(INV_NAME);
		assertNotEquals(st.getName(), INV_NAME);
		*/
	}
}