package org.mik.zoo;

/**
 * @author Maya Gillilan E0FAAA
 *
 */
public interface LivingBeing {
	
	public final static String TABLE_NAME = "living_being";
	
	public final static String COL_ID = "id";
	
	public final static String COL_SCIENTIFIC_NAME = "scientific_name";
	
	public final static String COL_INSTANCE_NAME = "instance_name";
	
	public final static String COL_IMG = "img";
	
	/**
	 * Simple interface declarations for basic methods
	 * @return
	 */
  public String getScientificName();

  public String getInstanceName();

  public String getImageURL();

}