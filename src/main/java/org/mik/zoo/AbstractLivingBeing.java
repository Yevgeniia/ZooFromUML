package org.mik.zoo;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author Maya Gillilan E0FAAA
 *
 */
public abstract class AbstractLivingBeing implements LivingBeing {

	/**
	 * Declarations of generic variables for all living beings
	 */
	private static List<LivingBeing> allLivingBeings = new ArrayList<>();
	
	private String scientificName;

	private String instanceName;

	private String imageURL;

	/**
	 * Generic constructor
	 * @param scientific Name
	 * @param instance Name
	 * @param image URL
	 */
	public AbstractLivingBeing(String scientificName, String instanceName,
			String imageURL) {
		this.scientificName = scientificName;
		this.instanceName = instanceName;
		this.imageURL = imageURL;
		allLivingBeings.add(this);
	}

	/**
	 * Constructor for an animal with no entered name or image
	 * @param scientific Name
	 */
	public AbstractLivingBeing(String scientificName) {
		this(scientificName,"",""); //$NON-NLS-1$ //$NON-NLS-2$
	}

	/**
	 * Getter and setter for the scientific name. Checks for null and empty entries
	 */
	public String getScientificName() {
		return this.scientificName;
	}

	public void setScientificName(String name) {
		if (name != null && !name.isEmpty())
			this.scientificName = name;
	}

	/**
	 * Getter and setter for instance name. Checks for null and empty entries
	 */
	public String getInstanceName() {
		return this.instanceName;
	}

	public void setInstanceName(String name) {
		if (name != null && !name.isEmpty())
			this.instanceName = name;
	}

	/**
	 * Getter and setter for the image. Checks for null and empty entries
	 */
	public String getImageURL() {
		return this.imageURL;
	}

	public void setImageURL(String url) {
		if (url != null && !url.isEmpty())
			this.imageURL = url;
	}
	
	/**
	 * Sets the list of living beings to be unmodifiable
	 * @return
	 */
	public static List<LivingBeing> getAllLivingBeings() {
		return Collections.unmodifiableList(allLivingBeings);
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((this.imageURL == null) ? 0 : this.imageURL.hashCode());
		result = prime * result + ((this.instanceName == null) ? 0 : this.instanceName.hashCode());
		result = prime * result + ((this.scientificName == null) ? 0 : this.scientificName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AbstractLivingBeing other = (AbstractLivingBeing) obj;
		if (this.imageURL == null) {
			if (other.imageURL != null)
				return false;
		} else if (!this.imageURL.equals(other.imageURL))
			return false;
		if (this.instanceName == null) {
			if (other.instanceName != null)
				return false;
		} else if (!this.instanceName.equals(other.instanceName))
			return false;
		if (this.scientificName == null) {
			if (other.scientificName != null)
				return false;
		} else if (!this.scientificName.equals(other.scientificName))
			return false;
		return true;
	}


	/**
	 * Returns the scientific name of the object as well as the instance name for printing
	 */
	@Override
	public String toString() {
		return this.scientificName + " (" + this.instanceName+")"; //$NON-NLS-1$ //$NON-NLS-2$
	}
	
}