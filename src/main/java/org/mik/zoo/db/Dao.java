//XDF26K
package org.mik.zoo.db;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.mik.zoo.LivingBeing;

/**
 * @author Maya Gillilan E0FAAA
 *
 */
public class Dao implements ZooDao {

	private final static Logger LOGGER = LogManager.getLogger(Dao.class);

	private Connection connection;

	@Override
	public boolean open() {
		LOGGER.debug("Enter open"); //$NON-NLS-1$
		if (this.connection!=null)
			return true;
		
		try {
			connect();
			checkTables();
			return true;
			
		} catch (Exception e) {
			LOGGER.error(String.format("open error %s", e.getMessage() )); //$NON-NLS-1$
			return false;
		}
	}

	@Override
	public void close() {
		LOGGER.debug("Enter close"); //$NON-NLS-1$
		try {
			if (this.connection != null)
				this.connection.close();
		} catch (Exception e) {
			LOGGER.error(String.format("close error %s", e.getMessage())); //$NON-NLS-1$
		}

	}

	@Override
	public boolean delete() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public LivingBeing getbyId(Integer id) {
		LOGGER.debug(String.format("enter getById (%d)", id)); //$NON-NLS-1$
		if (!open())
			return null;
		
		String sql = String.format("select * from %s where %s=%d", LivingBeing.TABLE_NAME, LivingBeing.COL_ID, id);
		
		try (Statement stmt = this.connection.createStatement()) {
			try(ResultSet rs = stmt.executeQuery(sql)) {
				String sn = rs.getString(LivingBeing.COL_SCIENTIFIC_NAME);
				return createInstance(rs, sn);
			}
			
		} catch (Exception e) {
			LOGGER.error(String.format("getById error (%d): %s", id, e.getMessage())); //$NON-NLS-1$
		}

		return id;
		return null;
	}

	private LivingBeing createInstance(ResultSet rs, String sn) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<LivingBeing> getAll() {
		LOGGER.debug("Enter getAll"); //$NON-NLS-1$
		List<LivingBeing> result = new ArrayList<>();
		if (!open())
			return result;
		
		String sql = String.format("select * from %s", LivingBeing.TABLE_NAME);
		try(Statement stmt = this.connection.createStatement()) {
			try(ResultSet rs = stmt.executeQuery(sql)){
				addResultList(rs, result);
				return result;
			}
			
		} catch (Exception e) {
			LOGGER.error(String.format("getAll error:%s", e.getMessage())); //$NON-NLS-1$
		}
		
		return null;
	}

	private void addResultList(ResultSet rs, List<LivingBeing> result) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<LivingBeing> getbyScientificName(String name) {
		LOGGER.debug(String.format("Enter getScientificName: %s", name)); //$NON-NLS-1$
		return getbyColumnName(name, LivingBeing.COL_SCIENTIFIC_NAME);
	}

	@Override
	public List<LivingBeing> getbyInstanceName(String name) {
		LOGGER.debug("Enter getInstanceName: %s", name); //$NON-NLS-1$
		return getbyColumnName(name, LivingBeing.COL_INSTANCE_NAME);
	}
	
	public List<LivingBeing> getbyColumnName(String name, String column) {
		LOGGER.debug(String.format("Enter getColumnName: %s", column, name)); //$NON-NLS-1$
		List<LivingBeing> result = new ArrayList<>();
		if (!open())
			return result;
		
		String sql = String.format("select * from %s where %s='%s'", LivingBeing.TABLE_NAME, column, name);
		try (Statement stmt=this.connection.createStatement()) {
			try(ResultSet rs = stmt.executeQuery(sql)) {
				addResultList(rs, result);
				return result;
			}
		}
		catch (Exception e) {
			LOGGER.error(String.format("getbyColumnName error: %s", e.getMessage())); //$NON-NLS-1$
			return result;
		}
	}

	@Override
	public LivingBeing persist(LivingBeing lb) {
		// TODO Auto-generated method stub
		return null;
	}

	private void connect() throws ClassNotFoundException, SQLException {
		LOGGER.debug("Enter connect"); //$NON-NLS-1$
		Class.forName("org.hsqldb.jdbc.JDBCDriver");
		String url = "jdbc:hsqldb:zoodb";
		this.connection = DriverManager.getConnection(url, "sa", "");
	}

	private void checkTables() throws SQLException {
		LOGGER.debug("Enter checkTables"); //$NON-NLS-1$

		DatabaseMetaData md = this.connection.getMetaData();

		try (ResultSet rs = md.getTables(null, null, LivingBeing.TABLE_NAME, null)) {
			while (rs.next()) {
				String tn = rs.getString("TABLE_NAME");
				if (tn.equals(LivingBeing.TABLE_NAME))
					return;
			}

		} catch (Exception e) {
			LOGGER.error(String.format("checkTables error:%s", e.getMessage())); //$NON-NLS-1$
		}

		LOGGER.info("Create table"); //$NON-NLS-1$

		try (Statement stmt = this.connection.createStatement()) {
			String sql = String.format("create table %s ( %s )", LivingBeing.TABLE_NAME, createColumnDefinitions());
			stmt.executeUpdate(sql);
		} catch (Exception e) {
			LOGGER.error(String.format("create table error:%s", e.getMessage())); //$NON-NLS-1$
			return;
		}

		initDb();
	}

	private String createColumnDefinitions() {
		return null;
		// TODO
	}

	private void initDb() {
		// TODO
	}

}
