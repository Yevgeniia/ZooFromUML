//XDF26K
package org.mik.zoo.db;

import java.util.List;

import org.mik.zoo.LivingBeing;

/**
 * @author Maya Gillilan E0FAAA
 *
 */
public interface ZooDao {
	
	boolean open();
	
	void close();
	
	LivingBeing getbyId(Integer id);
	
	List<LivingBeing> getAll();
	
	List<LivingBeing> getbyScientificName(String name);
	
	List<LivingBeing> getbyInstanceName(String name);
	
	boolean delete();
	
	LivingBeing persist(LivingBeing lb);


}
