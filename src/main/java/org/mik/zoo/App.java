package org.mik.zoo;

import org.mik.zoo.animal.AbstractAnimal;
import org.mik.zoo.factory.ZooCsvFactory;
import org.mik.zoo.factory.ZooDbFactory;
import org.mik.zoo.factory.ZooFactory;

/**
 * @author Maya Gillilan E0FAAA
 *
 */
public class App {
	
	private final static String APP_NAME = "zoo.jar";
	private final static String ARG_DATABASE = "hsqldb";
	/**
	 * Processes the data in the given file (in this case zoo.txt) and checks for a valid result
	 * @param f name
	 */
	public App(String fname) {
		ZooFactory zf;
		if (fname.equals(ARG_DATABASE)) {
			zf = new ZooDbFactory();
		}
		else {
			zf = new ZooCsvFactory(fname);
		}

		boolean result = zf.process();
		if (!result) {
			System.out.println("Cannot load data");
			System.exit(-1);
		}
	}
	/**
	 * Method for searching for a being by instance name
	 * @param Name
	 * @return
	 */
	private LivingBeing findByName(String name) {
		for (LivingBeing lb:AbstractLivingBeing.getAllLivingBeings()) {
			if (lb.getInstanceName().equalsIgnoreCase(name))
				return lb;
		}
		return null;
	}

	/**
	 * Series of questions which are outputted by App
	 */
	public void queries() {
		System.out.println(String.format("There are %d living beings in the zoo", 
				AbstractLivingBeing.getAllLivingBeings().size()));
		System.out.println(String.format("There are %d animals in the zoo", 
				AbstractAnimal.getAllAnimal().size()));
		System.out.println(String.format("Does Tux exist in the zoo: %s", 
				Boolean.valueOf(findByName("Tux")!=null)));
	}
	/**
	 * Exits if no valid file is given as an argument
	 */
	private static void usage() {
		System.out.println(String.format("usage %s <filename>", APP_NAME));
		System.exit(-1);
		
	}
	
	/**
	 * Initiates usage method if argument length is 0
	 * @param arg s
	 */
    public static void main( String[] args ) {
    	if (args.length == 0)
    		usage();
    	
    	App app = new App(args[0]);
    	app.queries();
    }
}
