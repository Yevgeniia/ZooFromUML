package org.mik.zoo.animal;

import org.mik.zoo.LivingBeing;

/**
 * @author Maya Gillilan E0FAAA
 *
 */
public interface Animal extends LivingBeing {

	/**
	 * Basic interface declarations for use in AbstractAnimal
	 * @return
	 */
  public int getNumberOfLegs();

  public int getNumberOfTeeth();

  public int getAverageWeight();

  public AnimalType getAnimalType();

  public boolean isBird();

  public boolean isFish();

  public boolean isMammal();

}