package org.mik.zoo.animal.bird;

import org.mik.zoo.animal.Animal;

/**
 * @author Maya Gillilan E0FAAA
 *
 */
public interface Bird extends Animal {

  public int getWingLength();

}