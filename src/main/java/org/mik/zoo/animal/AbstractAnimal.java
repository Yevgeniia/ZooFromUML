package org.mik.zoo.animal;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.mik.zoo.AbstractLivingBeing;

/**
 * @author Maya Gillilan E0FAAA
 *
 */
public abstract class AbstractAnimal extends AbstractLivingBeing implements Animal {

	/**
	 * Declarations of maximums for validity checks, as well as the new variables for this class
	 */
	public final static int MAX_LEGS = 1000;
	public final static int MAX_TEETH = 500;
	public final static int MAX_WEIGHT = 5000; 
	
	private static List<Animal> allAnimal = new ArrayList<>();
	
	private int numberOfLegs;

	private int numberOfTeeth;

	private int averageWeight;

	private AnimalType animalType;

	/**
	 * Constructor for an Animal with all variables known, as well as adding the animal to the list of animals
	 * @param scientific Name
	 * @param instance Name
	 * @param image URL
	 * @param numberOf Legs
	 * @param numberOf Teeth
	 * @param average Weight
	 * @param animal Type
	 */
	public AbstractAnimal(String scientificName, String instanceName, 
			String imageURL, int numberOfLegs,
			int numberOfTeeth, int averageWeight, AnimalType animalType) {
		super(scientificName, instanceName, imageURL);
		this.numberOfLegs = numberOfLegs;
		this.numberOfTeeth = numberOfTeeth;
		this.averageWeight = averageWeight;
		this.animalType = animalType;
		allAnimal.add(this);
	}

	/**
	 * Constructor for an Animal without an instance name and/or image
	 * @param scientific Name
	 * @param numberOf Legs
	 * @param numberOf Teeth
	 * @param average Weight
	 * @param animal Type
	 */
	public AbstractAnimal(String scientificName, int numberOfLegs, 
			int numberOfTeeth, int averageWeight,
			AnimalType animalType) {
		this(scientificName, "", "", numberOfLegs, numberOfTeeth, //$NON-NLS-1$ //$NON-NLS-2$
				averageWeight, animalType);
	}

	/**
	 * Getter and setter for number of legs, checks to make sure that number is below the max and not negative
	 */

	@Override
	public int getNumberOfLegs() {
		return this.numberOfLegs;
	}
	
	public void setNumberOfLegs(int l) {
		if (l>=0 && l<= MAX_LEGS)
			this.numberOfLegs = l;
	}

	/**
	 * Getter and setter for number of teeth, checks to make sure that number is below the max and not negative
	 */
	@Override
	public int getNumberOfTeeth() {
		return this.numberOfTeeth;
	}

	public void setNumberOfTeeth(int t) {
		if (t>=0 && t<= MAX_TEETH)
			this.numberOfTeeth = t;
	}

	/** 
	 * Getter and setter for average weight, checks to make sure that number is between 0 and the max
	 */
	@Override
	public int getAverageWeight() {
		return this.averageWeight;
	}

	public void setAverageWeight(int w) {
		if (w>0 && w<=MAX_WEIGHT)
			this.averageWeight = w;
	}

	/**
	 * Getter and setter for animal type, as long as entry is not null
	 */
	@Override
	public AnimalType getAnimalType() {
		return this.animalType;
	}

	public void setAnimalType(AnimalType t) {
		if (t!=null)
			this.animalType = t;
	}

	/**
	 * Basic boolean declarations for inheritance
	 */
	@Override
	public boolean isFish() {
		return false;
	}

	@Override
	public boolean isMammal() {
		return false;
	}
	
	@Override
	public boolean isBird() {
		return false;
	}

	/**
	 * Makes the list of animals unmodifiable
	 * @return
	 */
	public static List<Animal> getAllAnimal() {
		return Collections.unmodifiableList(allAnimal);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((this.animalType == null) ? 0 : this.animalType.hashCode());
		result = prime * result + this.averageWeight;
		result = prime * result + this.numberOfLegs;
		result = prime * result + this.numberOfTeeth;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		AbstractAnimal other = (AbstractAnimal) obj;
		if (this.animalType != other.animalType)
			return false;
		if (this.averageWeight != other.averageWeight)
			return false;
		if (this.numberOfLegs != other.numberOfLegs)
			return false;
		if (this.numberOfTeeth != other.numberOfTeeth)
			return false;
		return true;
	}

	
}