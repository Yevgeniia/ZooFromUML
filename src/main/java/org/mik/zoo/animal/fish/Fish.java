package org.mik.zoo.animal.fish;

import org.mik.zoo.animal.Animal;

/**
 * @author Maya Gillilan E0FAAA
 *
 */
public interface Fish extends Animal {

  public int getDeep();

}