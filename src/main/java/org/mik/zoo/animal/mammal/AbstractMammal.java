package org.mik.zoo.animal.mammal;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.mik.zoo.animal.AbstractAnimal;
import org.mik.zoo.animal.AnimalType;

/**
 * @author Maya Gillilan E0FAAA
 *
 */
public abstract class AbstractMammal extends AbstractAnimal implements Mammal {

	private static List<Mammal> allMammal = new ArrayList<>();
	
	private int hairLength;
	/**
	 * Constructor for a mammal with all variables known
	 * @param scientific Name
	 * @param instance Name
	 * @param image URL
	 * @param numberOf Legs
	 * @param numberOf Teeth
	 * @param average Weight
	 * @param animal Type
	 * @param hair Length
	 */
	public AbstractMammal(String scientificName, String instanceName, 
			String imageURL, int numberOfLegs, int numberOfTeeth, 
			int averageWeight, AnimalType animalType, int hairLength) {
		super(scientificName, instanceName, imageURL, numberOfLegs, numberOfTeeth, averageWeight, animalType);
		this.hairLength = hairLength;
		allMammal.add(this);
	}

	/**
	 * Constructor for a mammal without a known instance name and image
	 * @param scientific Name
	 * @param numberOf Legs
	 * @param numberOf Teeth
	 * @param average Weight
	 * @param animal Type
	 * @param hair Length
	 */
	public AbstractMammal(String scientificName, int numberOfLegs,
			int numberOfTeeth, int averageWeight, AnimalType animalType, int hairLength) {
		this(scientificName, "", "", numberOfLegs, numberOfTeeth,  //$NON-NLS-1$ //$NON-NLS-2$
				averageWeight, animalType, hairLength);
	}

	/**
	 * Getter and setter for the new variable hair length, as long as it is not negative
	 */
	@Override
	public int getHairLength() {
		return this.hairLength;
	}

	public void setHairLength(int h) {
		if (h >= 0)
			this.hairLength = h;
	}

	/**
	 * Makes the list of mammals unmodifiable
	 * @return
	 */
	public static List<Mammal> getAllMammal() {
		return Collections.unmodifiableList(allMammal);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + this.hairLength;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		AbstractMammal other = (AbstractMammal) obj;
		if (this.hairLength != other.hairLength)
			return false;
		return true;
	}

	
}