package org.mik.zoo.animal.mammal;

import java.util.Scanner;

import org.mik.zoo.animal.AnimalType;

/**
 * @author Maya Gillilan E0FAAA
 *
 */
public class Elephant extends AbstractMammal {

	/**
	 * Sets constant values for the inherited variables
	 */
	public final static String SCIENTIFIC_NAME = "Elephant";
	public final static int NUMBER_OF_LEGS = 4;
	public final static int NUMBER_OF_TEETH = 26;
	public final static int AVERAGE_WEIGHT = 5000;
	public final static int HAIR_LENGTH = 5;
	public final static String IMAGE_URL = "elephant.png";
	
	
	public Elephant() {
		this("");
	}
	/**
	 * Inherited constructor for an elephant with an instance name
	 * @param instance Name
	 */
	public Elephant(String instanceName) {
		super(SCIENTIFIC_NAME, instanceName, IMAGE_URL, NUMBER_OF_LEGS, NUMBER_OF_TEETH,
				AVERAGE_WEIGHT, AnimalType.HERBIVOROUS, HAIR_LENGTH);
	}
	
	/**
	 * Inherited constructor for an elephant with a name and image but no instance name
	 * @param name
	 * @param image
	 */
	public Elephant(String name, String image) {
		super(SCIENTIFIC_NAME, name, image, NUMBER_OF_LEGS, NUMBER_OF_TEETH, AVERAGE_WEIGHT, 
				AnimalType.HERBIVOROUS, HAIR_LENGTH);
	}
	
	/**
	 * Appends the species (Elephant), number of legs, and number off teeth to the inherited output
	 */
	@Override
	public String toString() {	
		return "Elephant:" + super.toString() 
		 + " legs:" + this.getNumberOfLegs() 
		 + " teeth:" + this.getNumberOfTeeth();
	}	
	/**
	 * Scans for a new elephant instance. As long as the image exists, the information will be stored. Otherwise, it will
	 * return null or an error (if present)
	 * @param scanner
	 * @return
	 */
	public static Elephant factoryMethod(Scanner scanner) {
		try {
			String name = scanner.next();
			String image = scanner.next();
			if (image == null)
				return null;
			
			return new Elephant(name, image);
		}
		catch (Exception e) {
			System.out.println("Elephant.factoryMethod: "+e.getMessage());
			return null;
		}
	}
	
}