package org.mik.zoo.animal.mammal;

import org.mik.zoo.animal.Animal;

/**
 * @author Maya Gillilan E0FAAA
 *
 */
public interface Mammal extends Animal {

  public int getHairLength();

}