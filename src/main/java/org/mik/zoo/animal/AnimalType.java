package org.mik.zoo.animal;

/**
 * @author Maya Gillilan E0FAAA
 *
 */
public enum AnimalType {

  PREDATOR,

  HERBIVOROUS;

}