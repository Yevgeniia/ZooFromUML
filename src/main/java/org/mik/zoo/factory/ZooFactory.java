//XDF26K
package org.mik.zoo.factory;

import org.mik.zoo.db.ZooDao;

/**
 * @author Maya Gillilan E0FAAA
 *
 */
public interface ZooFactory {
	
	boolean process();
	
	int getLines();
	
	int getErrors();
	
	ZooDao getDao();
}
