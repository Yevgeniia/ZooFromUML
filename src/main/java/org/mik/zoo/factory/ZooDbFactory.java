//XDF26K
package org.mik.zoo.factory;

import java.util.List;

import org.mik.zoo.LivingBeing;
import org.mik.zoo.db.Dao;
import org.mik.zoo.db.ZooDao;

/**
 * @author Maya Gillilan E0FAAA
 *
 */
public class ZooDbFactory implements ZooFactory {

	private int lines;
	private ZooDao dao;
	
	
	public ZooDbFactory() {
		this.dao = new Dao();
	}
	
	@Override
	public boolean process() {
		List<LivingBeing> result = this.dao.getAll();
		if (result!=null)
			this.lines = result.size();
		
		return false;
	}

	@Override
	public int getLines() {
		return this.lines;
	}

	@Override
	public int getErrors() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public ZooDao getDao() {
		return this.dao;
	}

}
