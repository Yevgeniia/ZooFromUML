//XDF26K
package org.mik.zoo.factory;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Scanner;

import org.mik.zoo.LivingBeing;
import org.mik.zoo.animal.mammal.Elephant;

/**
 * @author Maya Gillilan E0FAAA
 *
 */
public class ZooCsvFactory implements ZooFactory {
	
	/**
	 * Sets the delimiter to match what is used in the file
	 */
	private final static String FIELD_DELIMITER = ";";
	
	private String fName;
	private int lines;
	private int errors;
	
	public ZooCsvFactory(String fName) {
		this.fName = fName;
	}
	/**
	 * Processes an entry, iterating over all lines, and if any errors are found, logs them and prints an error
	 * @return
	 */
	@Override
	public boolean process() {
		try {
			this.lines = 0;
			this.errors = 0;
			for(String s:Files.readAllLines(Paths.get(this.fName))) {
				if(this.lines++==0)
					continue;
				if (!processLine(s))
					++this.errors;
			}
			return true;
		} catch (Exception e) {
			System.out.println("ZooFactory.process: "+e.getMessage());
			return false;
		}
	}
	
	@Override
	public int getLines() {
		return lines;
	}

	@Override
	public int getErrors() {
		return errors;
	}

	/**
	 * Method to process a line, using delimiters to read properly. Will return a living being if it is not null
	 * @param line
	 * @return
	 */
	private boolean processLine(String line) {
		try(Scanner scanner = new Scanner(line)) {
			scanner.useDelimiter(FIELD_DELIMITER);
			if (!scanner.hasNext())
				return false;
			return createLivingBeing(scanner) != null;
		}
		catch (Exception e) {
			System.out.println("ZooFactory.processLine error: "+e.getMessage());
			return false;
		}
	}

	/**
	 * Creates a living being if found in file
	 * @param scanner
	 * @return
	 */
	private LivingBeing createLivingBeing(Scanner scanner) {
		try {
			String tag = scanner.next();
			if (!scanner.hasNext())
				return null;
			switch (tag) {
			case Elephant.SCIENTIFIC_NAME: return Elephant.factoryMethod(scanner);

			default:
				System.out.println("Unknown tag found: "+tag);
				return null;
			}
			
		} catch (Exception e) {
			System.out.println("ZooFactory.createLivingBeing error: "+e.getMessage());
			return null;
		}
		
	}
	@Override
	public ZooDao getDao() {
		return null;
	}
}
